#! /usr/bin/env python3


import svgutils.transform as sg
import sys 


#create new SVG figure
fig = sg.SVGFigure("12cm", "20cm")


for i in sys.argv[1:]:
    fig1 = sg.fromfile(i)

    plot1 = fig1.getroot()

    group1 = sg.GroupElement([plot1], attrib={"{http://www.inkscape.org/namespaces/inkscape}label": "Layer 1", "{http://www.inkscape.org/namespaces/inkscape}groupmode": "layer"})

    fig.append([group1])

# save generated SVG files
fig.save("fig_final.svg")

